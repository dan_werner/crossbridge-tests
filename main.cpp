#ifndef USE_IOSTREAM
#include <stdio.h>
#else
#include <sstream>
#endif

#ifdef NO_VIRTUAL_METHODS
#define VIRTUAL 
#else
#define VIRTUAL virtual
#endif

struct LogRecursion {
	LogRecursion(): iter(0) {}
	int iter;
	void log() {
#ifdef USE_IOSTREAM
		std::cout << iter << "\n";
#else
		printf("%i\n", iter);
#endif
	}

	VIRTUAL int Fibonacci(int n) {
		if ( n == 0 )
			return 0;
		else if ( n == 1 )
			return 1;
		else
			return ( Fibonacci(n-1) + Fibonacci(n-2) );
	} 

};

struct A : LogRecursion { virtual int Fibonacci(int n){ return LogRecursion::Fibonacci(n); } };
struct B : A { virtual int Fibonacci(int n){ return A::Fibonacci(n); } };
struct C : B { virtual int Fibonacci(int n){ return B::Fibonacci(n); } };
struct D : C { virtual int Fibonacci(int n){ return C::Fibonacci(n); } };

int main(int argc, const char **argv) {
#ifdef NO_VIRTUAL_METHODS
	LogRecursion lr;
#else
	D lr;
#endif
	for (int i = 0; i < 42; i++) {
		lr.iter = lr.Fibonacci(i);
	}
	lr.log();
}





